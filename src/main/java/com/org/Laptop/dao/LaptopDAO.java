package com.org.Laptop.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.org.Laptop.dto.LaptopDTO;


public class LaptopDAO {
	private static LaptopDAO sf=null;
	private Configuration configuration=null;
	private SessionFactory sef=null;
	private LaptopDAO() {
		 configuration = new Configuration();
			configuration.configure("config.xml");
			//configuration.addAnnotatedClass(LaptopDTO.class);
			sef=configuration.buildSessionFactory();
	}
	public static LaptopDAO getInstance(){
		if(sf==null) { 
			sf=new LaptopDAO();
		return sf;}
		return sf;
	}
public void saveLaptopDetails(LaptopDTO lpt) {
	Session session = sef.openSession();
	Transaction transaction = session.beginTransaction();
	session.save(lpt);
	transaction.commit();
}
	
	public LaptopDTO getLaptopDetailsByLid(Long lid) {
		Session session = sef.openSession();
		String hql="from LaptopDTO where lid=:lno";
		Query query = session.createQuery(hql);
		query.setParameter("lno", lid );
		LaptopDTO uniqueResult = (LaptopDTO) query.uniqueResult();
		return uniqueResult;
	}
/*public void updateColorByLid(String color,Long lid) {
	Session session = sef.openSession();
	Transaction transaction = session.beginTransaction();
	String hql="update LaptopDTO set color=:newColor where lid=:lno";
	Query query = session.createQuery(hql);
	query.setParameter("newColor", color);
	query.setParameter("lno", lid);
	int updateRows = query.executeUpdate();
	transaction.commit();
	if(updateRows == 0) {
		System.out.println("Update Operation Failed");
		return;
	}
	System.out.println("Update Operation successfull");
}*/

public void deleteByLid(Long lid) {
Session session = sef.openSession();
Transaction transaction = session.beginTransaction();
String hql="delete LaptopDTO where lid=:lno";
Query query = session.createQuery(hql);
query.setParameter("lno", lid);
int updateRows = query.executeUpdate();
transaction.commit();
if(updateRows == 0) {
	System.out.println("Delete Operation Failed");
	return;
}
System.out.println("Delete Operation successfull");

}
}



