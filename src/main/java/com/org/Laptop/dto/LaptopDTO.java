package com.org.Laptop.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="laptop_details")
public class LaptopDTO implements Serializable {
	@Id
	@GenericGenerator(name="lid_auto", strategy ="increment")
	@GeneratedValue(generator ="lid_auto")
	@Column(name="LID")
	private Long lid;
	@Column(name="NAME")
	private String name;
	@Column(name="PROCESSOR")
	private String processor;
	@Column(name="SCREEN_SIZE")
	private String screenSize;
	@Column(name="RAM")
	private String ram;
	@Column(name="COLOR")
	private String color;
	@Column(name="PRICE")
	private Double price;
	
	public LaptopDTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getLid() {
		return lid;
	}

	public void setLid(Long lid) {
		this.lid = lid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getScreenSize() {
		return screenSize;
	}

	public void setScreenSize(String screenSize) {
		this.screenSize = screenSize;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "LaptopDTO [lid=" + lid + ", name=" + name + ", processor=" + processor + ", screenSize=" + screenSize
				+ ", ram=" + ram + ", color=" + color + ", price=" + price + "]";
	}
	
	
	

}
